import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOneInstrumentComponent } from './show-one-instrument.component';

describe('ShowOneInstrumentComponent', () => {
  let component: ShowOneInstrumentComponent;
  let fixture: ComponentFixture<ShowOneInstrumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowOneInstrumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOneInstrumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
