import { Component, OnInit } from '@angular/core';
import {NetworkService} from '../network-service.service';
import {ActivatedRoute, Params, Router} from "@angular/router";

@Component({
  selector: 'app-show-one-instrument',
  templateUrl: './show-one-instrument.component.html',
  styleUrls: ['./show-one-instrument.component.css']
})
export class ShowOneInstrumentComponent implements OnInit {

  instrument: any;
  id;
  constructor(
    private networkService: NetworkService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {


    // this.route.snapshot.subscribe(
    //     (params: Params) => {
    //       this.id = (params['id']);
    //     }
    //   );
    //
    // this.getInstrumentById(this.id);
  }

  getInstrumentById(id) {
    this.networkService.getInstrumentById(id).subscribe((data: any) => {
        if (data.status === 'Success') {
          console.log(data);
          this.instrument = data.data;
        }
      },
      (errorData: any) => {
        console.log(errorData);
        const error = JSON.parse(errorData._body);
        // this.loginForm.reset();
        // const modalRef = this.modalService.open(SignUpErrorModal);
        // modalRef.componentInstance.title = error.status;
        // modalRef.componentInstance.message = error.message;
      });
  }

}
