import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAllInstrumentComponent } from './show-all-instrument.component';

describe('ShowAllInstrumentComponent', () => {
  let component: ShowAllInstrumentComponent;
  let fixture: ComponentFixture<ShowAllInstrumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAllInstrumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAllInstrumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
