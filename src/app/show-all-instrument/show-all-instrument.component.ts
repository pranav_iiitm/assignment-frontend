import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NetworkService} from "../network-service.service";

@Component({
  selector: 'app-show-all-instrument',
  templateUrl: './show-all-instrument.component.html',
  styleUrls: ['./show-all-instrument.component.css']
})
export class ShowAllInstrumentComponent implements OnInit, AfterViewInit {

  instrumentList: [{}];

  constructor(private networkService: NetworkService) { }

  ngOnInit() {

    this.getInstrumentList();
    setTimeout(
      function(){
        location.reload();
      }, 5000);
  }


  getInstrumentList() {
    this.networkService.getInstrument().subscribe((data: any) => {
        if (data.status === 'Success') {
          console.log(data);
          this.instrumentList = data.data;
        }
      },
      (errorData: any) => {
        console.log(errorData);
        const error = JSON.parse(errorData._body);
        // this.loginForm.reset();
        // const modalRef = this.modalService.open(SignUpErrorModal);
        // modalRef.componentInstance.title = error.status;
        // modalRef.componentInstance.message = error.message;
      });
  }

  ngAfterViewInit() {
  $(function() {
    $(".preloader").fadeOut();
  });
  $(function() {
    (<any>$('[data-toggle="tooltip"]')).tooltip()
  });
  $('#to-recover').on("click", function() {
    $("#loginform").slideUp();
    $("#recoverform").fadeIn();
  });
}



}
