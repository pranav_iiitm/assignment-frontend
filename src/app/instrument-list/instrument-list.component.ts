import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NetworkService} from '../network-service.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-instrument-list',
  templateUrl: './instrument-list.component.html',
  styleUrls: ['./instrument-list.component.css']
})
export class InstrumentListComponent implements OnInit, AfterViewInit {

  instrumentList: [{}] ;
  isEnquiryDone = false;
  instrumentElement: any;

  enquiryInstrumentForm: FormGroup;

  constructor(
    private networkService: NetworkService
  ) { }

  ngOnInit() {

    this.enquiryInstrumentForm = new FormGroup({
      instrumentId: new FormControl(null, [Validators.required]),
      rawData: new FormControl(null, [Validators.required])
    });

    this.getInstrumentList();
  }

  getInstrumentList() {
    this.networkService.getInstrument().subscribe((data: any) => {
        if (data.status === 'Success') {
          console.log(data);
          this.instrumentList = data.data;
        }
      },
      (errorData: any) => {
        console.log(errorData);
        const error = JSON.parse(errorData._body);
        // this.loginForm.reset();
        // const modalRef = this.modalService.open(SignUpErrorModal);
        // modalRef.componentInstance.title = error.status;
        // modalRef.componentInstance.message = error.message;
      });
  }


  getInstrumentById(id) {
    this.networkService.getInstrumentById(id).subscribe((data: any) => {
        if (data.status === 'Success') {
          console.log(data);
          this.instrumentElement = data.data;
          this.isEnquiryDone = true;
        }
      },
      (errorData: any) => {
        console.log(errorData);
        const error = JSON.parse(errorData._body);
        // this.loginForm.reset();
        // const modalRef = this.modalService.open(SignUpErrorModal);
        // modalRef.componentInstance.title = error.status;
        // modalRef.componentInstance.message = error.message;
      });
  }

  createInstrument(rawData) {
    this.networkService.createInstrument(rawData).subscribe((data: any) => {
        if (data.status === 'Success') {
          console.log(data);
          this.instrumentElement = data.data;
          this.isEnquiryDone = true;
        }
      },
      (errorData: any) => {
        console.log(errorData);
        const error = JSON.parse(errorData._body);
        // this.loginForm.reset();
        // const modalRef = this.modalService.open(SignUpErrorModal);
        // modalRef.componentInstance.title = error.status;
        // modalRef.componentInstance.message = error.message;
      });
  }


  ngAfterViewInit() {
    $(function() {
      $(".preloader").fadeOut();
    });
    $(function() {
      (<any>$('[data-toggle="tooltip"]')).tooltip()
    });
    $('#to-recover').on("click", function() {
      $("#loginform").slideUp();
      $("#recoverform").fadeIn();
    });
  }




}
