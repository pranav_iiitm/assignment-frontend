import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/auth.guard';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InstrumentListComponent } from './instrument-list/instrument-list.component';
import {NetworkService} from './network-service.service';
import { ShowAllInstrumentComponent } from './show-all-instrument/show-all-instrument.component';
import { ShowOneInstrumentComponent } from './show-one-instrument/show-one-instrument.component';


@NgModule({
  declarations: [
    AppComponent,
    InstrumentListComponent,
    ShowAllInstrumentComponent,
    ShowOneInstrumentComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [AuthGuard, NetworkService],
  bootstrap: [AppComponent]
})
export class AppModule { }
