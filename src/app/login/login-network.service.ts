
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {environment} from "../../environments/environment";

@Injectable()
export class LoginNetworkService {

  BASE_URL = environment.BASE_URL;

  constructor(
    private http: Http) { }

  login(userName: string, password: string) {

    const url = `${this.BASE_URL}/api/v1/login`;

    return this.http.post(url,
      {
        'userName': userName,
        'password': password
      }
    ).map((response: Response) => {
      console.log(response);
      const headers = response.headers.get('token');
      const responseData = response.json();
      responseData.headers = headers;
      return responseData;
    });

  }


}
