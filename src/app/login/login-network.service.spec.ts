import { TestBed, inject } from '@angular/core/testing';

import { LoginNetworkService } from './login-network.service';

describe('LoginNetworkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginNetworkService]
    });
  });

  it('should be created', inject([LoginNetworkService], (service: LoginNetworkService) => {
    expect(service).toBeTruthy();
  }));
});
