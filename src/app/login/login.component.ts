import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginNetworkService} from "./login-network.service";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;

    constructor(public router: Router,
                public loginNetworkService: LoginNetworkService) {}

    ngOnInit() {
      this.loginForm = new FormGroup({
        userName: new FormControl(null, [Validators.required, Validators.pattern(/^\d{10}$/)]),
        password: new FormControl(null, Validators.required)
      });
    }


  navigateToDashboard(){
      this.router.navigate(['/']);
  }

  onSubmit(userName: string, password: string){
    console.log(userName);
    console.log(password);

      this.loginNetworkService.login(userName, password).subscribe((data: any) => {
          if (data.status === 'Success') {
            console.log(data);
            if (data.headers){
              localStorage.setItem('token', data.headers);
              this.navigateToDashboard();
            }
          }
        },
        (errorData: any) => {
          console.log(errorData);
          const error = JSON.parse(errorData._body);
          this.loginForm.reset();
          // const modalRef = this.modalService.open(SignUpErrorModal);
          // modalRef.componentInstance.title = error.status;
          // modalRef.componentInstance.message = error.message;
        });
  }



  ngAfterViewInit() {
    $(function() {
      $(".preloader").fadeOut();
    });
    $(function() {
      (<any>$('[data-toggle="tooltip"]')).tooltip()
    });
    $('#to-recover').on("click", function() {
      $("#loginform").slideUp();
      $("#recoverform").fadeIn();
    });
  }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

}
