import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from './shared/guard/auth.guard';
import {InstrumentListComponent} from './instrument-list/instrument-list.component';
import {ShowAllInstrumentComponent} from './show-all-instrument/show-all-instrument.component';
import {ShowOneInstrumentComponent} from './show-one-instrument/show-one-instrument.component';
// import {ShowAllInstrumentComponent} from "./show-all-instrument/show-all-instrument.component";

const routes: Routes = [
    { path: '', component: InstrumentListComponent, canActivate: [AuthGuard]},
    { path: 'showAll', component: ShowAllInstrumentComponent, canActivate: [AuthGuard] },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path: '404', loadChildren: './404/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: '404' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes), NgbModule.forRoot()],
    exports: [RouterModule]
})
export class AppRoutingModule { }

