import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SignupNetworkService} from './signup-network.service';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    ReactiveFormsModule,
  ],
  providers:[SignupNetworkService],
  declarations: [SignupComponent],

})
export class SignupModule { }
