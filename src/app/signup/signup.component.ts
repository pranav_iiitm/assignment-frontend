import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SignupNetworkService} from "./signup-network.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, AfterViewInit {


    signUpForm: FormGroup;

    constructor(
      private router: Router,
      private route: ActivatedRoute,
      private signupNetworkService:  SignupNetworkService
    ) { }

    ngOnInit() {

      this.signUpForm = new FormGroup({
        name : new FormControl(null, Validators.required),
        mobile : new FormControl(null, [Validators.required, Validators.pattern(/^\d{10}$/)]),
        password : new FormControl(null, [Validators.required, Validators.minLength(6)])
      });
    }


    navigateToLogin(){
      this.router.navigate(['/login']);
    }

  onSubmit(name, mobile, password) {
    console.log(name);
    console.log(mobile);
    console.log(password);

    this.signupNetworkService.signup(name, mobile, password).subscribe(
      (data: any) => {
        console.log(data);

        if (data.status === 'Success') {
            this.navigateToLogin();
          // this.mobileNumberToVerify = this.signUpFormGroup.value.username;
          // this.otpVerificationFormGroup = new FormGroup({
          //   otp: new FormControl('', Validators.required)
          // });
        }

      },
      (errorData: any) => {
        console.log(errorData);
        const error = JSON.parse(errorData._body);
        // const modalRef = this.modalService.open(SignUpErrorModal);
        // modalRef.componentInstance.title = error.status;
        // modalRef.componentInstance.message = error.message;

      }
    );



  }





  ngAfterViewInit() {
    $(function() {
      $(".preloader").fadeOut();
    });
    $(function() {
      (<any>$('[data-toggle="tooltip"]')).tooltip()
    });
    $('#to-recover').on("click", function() {
      $("#loginform").slideUp();
      $("#recoverform").fadeIn();
    });
  }
}
