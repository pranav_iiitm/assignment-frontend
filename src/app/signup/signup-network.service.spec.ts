import { TestBed, inject } from '@angular/core/testing';

import { SignupNetworkService } from './signup-network.service';

describe('SignupNetworkService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignupNetworkService]
    });
  });

  it('should be created', inject([SignupNetworkService], (service: SignupNetworkService) => {
    expect(service).toBeTruthy();
  }));
});
