
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {environment} from '../../environments/environment';

@Injectable()
export class SignupNetworkService {

  BASE_URL = environment.BASE_URL;

  constructor(
    private http: Http) { }

  signup(name: string, mobile: string, password: string) {

    const url = `${this.BASE_URL}/api/v1/signUp`;

    return this.http.post(url,
      {
        'name' : name,
        'mobile' : mobile,
        'password': password
      }
    ).map((response: Response) => {
      return response.json();
    });

  }


}
