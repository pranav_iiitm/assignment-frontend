
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {environment} from '../environments/environment';

@Injectable()
export class NetworkService {

  BASE_URL = environment.BASE_URL;

  constructor(
    private http: Http) { }



  createInstrument(rawData: string) {

    var header = new Headers();
    header.append('token', localStorage.getItem('token'));

    return this.http.post(
      `${environment.BASE_URL}/api/v1/instrument`,
      {
        rawData : rawData
      },
      {
        headers: header
      }
    ).map(
      (response: Response) => {
        return response.json();
      }
    );

  }

  getInstrument() {

    var header = new Headers();
    header.append('token', localStorage.getItem('token'));

    const url = `${this.BASE_URL}/api/v1/instrument`;

    return this.http.get(url,
      {
        headers: header
      }
    ).map((response: Response) => {
      console.log(response);
      const headers = response.headers.get('token');
      const responseData = response.json();
      responseData.headers = headers;
      return responseData;
    });

  }

  getInstrumentById(id) {

    var header = new Headers();
    header.append('token', localStorage.getItem('token'));

    const url = `${this.BASE_URL}/api/v1/instrument/${id}`;

    return this.http.get(url,
      {
        headers: header
      }
    ).map((response: Response) => {
      console.log(response);
      const headers = response.headers.get('token');
      const responseData = response.json();
      responseData.headers = headers;
      return responseData;
    });

  }







}
